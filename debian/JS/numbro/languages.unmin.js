exports["bg"] = {
    "languageTag": "bg",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "И",
        "million": "А",
        "billion": "M",
        "trillion": "T"
    },
    "currency": {
        "symbol": "лв.",
        "code": "BGN"
    }
};
exports["cs-CZ"] = {
    "languageTag": "cs-CZ",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "tis.",
        "million": "mil.",
        "billion": "mld.",
        "trillion": "bil."
    },
    "spaceSeparated": true,
    "currency": {
        "symbol": "Kč",
        "position": "postfix",
        "code": "CZK"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "spaceSeparatedAbbreviation": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["da-DK"] = {
    "languageTag": "da-DK",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "t",
        "million": "mio",
        "billion": "mia",
        "trillion": "b"
    },
    "currency": {
        "symbol": "kr",
        "position": "postfix",
        "code": "DKK"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["de-AT"] = {
    "languageTag": "de-AT",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "€",
        "code": "EUR"
    }
};
exports["de-CH"] = {
    "languageTag": "de-CH",
    "delimiters": {
        "thousands": "’",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "CHF",
        "position": "postfix",
        "code": "CHF"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["de-DE"] = {
    "languageTag": "de-DE",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "Mi",
        "billion": "Ma",
        "trillion": "Bi"
    },
    "spaceSeparated": true,
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "totalLength": 4,
        "thousandSeparated": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["de-LI"] = {
    "languageTag": "de-LI",
    "delimiters": {
        "thousands": "'",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "CHF",
        "position": "postfix",
        "code": "CHF"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["el"] = {
    "languageTag": "el",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "χ",
        "million": "ε",
        "billion": "δ",
        "trillion": "τ"
    },
    "currency": {
        "symbol": "€",
        "code": "EUR"
    }
};
exports["en-AU"] = {
    "languageTag": "en-AU",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "$",
        "position": "prefix",
        "code": "AUD"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "thousandSeparated": true,
            "mantissa": 2
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["en-GB"] = {
    "languageTag": "en-GB",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "£",
        "position": "prefix",
        "code": "GBP"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": false,
        "spaceSeparatedCurrency": false,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": false,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "thousandSeparated": true,
            "spaceSeparated": false,
            "mantissa": 2
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "thousandSeparated": true,
            "spaceSeparated": false,
            "mantissa": 0
        }
    }
};
exports["en-IE"] = {
    "languageTag": "en-IE",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "€",
        "position": "prefix",
        "code": "EUR"
    }
};
exports["en-NZ"] = {
    "languageTag": "en-NZ",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "$",
        "position": "prefix",
        "code": "NZD"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "thousandSeparated": true,
            "mantissa": 2
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["en-ZA"] = {
    "languageTag": "en-ZA",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "R",
        "position": "prefix",
        "code": "ZAR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "thousandSeparated": true,
            "mantissa": 2
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-AR"] = {
    "languageTag": "es-AR",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "$",
        "position": "postfix",
        "code": "ARS"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-CL"] = {
    "languageTag": "es-CL",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "$",
        "position": "prefix",
        "code": "CLP"
    },
    "currencyFormat": {
        "output": "currency",
        "thousandSeparated": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-CO"] = {
    "languageTag": "es-CO",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-CR"] = {
    "languageTag": "es-CR",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "₡",
        "position": "postfix",
        "code": "CRC"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-ES"] = {
    "languageTag": "es-ES",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-MX"] = {
    "languageTag": "es-MX",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "$",
        "position": "postfix",
        "code": "MXN"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-NI"] = {
    "languageTag": "es-NI",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "C$",
        "position": "prefix",
        "code": "NIO"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-PE"] = {
    "languageTag": "es-PE",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "S/.",
        "position": "prefix",
        "code": "PEN"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-PR"] = {
    "languageTag": "es-PR",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "$",
        "position": "prefix",
        "code": "USD"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["es-SV"] = {
    "languageTag": "es-SV",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mm",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "$",
        "position": "prefix",
        "code": "SVC"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["et-EE"] = {
    "languageTag": "et-EE",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "tuh",
        "million": "mln",
        "billion": "mld",
        "trillion": "trl"
    },
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["fa-IR"] = {
    "languageTag": "fa-IR",
    "delimiters": {
        "thousands": "،",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "هزار",
        "million": "میلیون",
        "billion": "میلیارد",
        "trillion": "تریلیون"
    },
    "currency": {
        "symbol": "﷼",
        "code": "IRR"
    }
};
exports["fi-FI"] = {
    "languageTag": "fi-FI",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "M",
        "billion": "G",
        "trillion": "T"
    },
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["fil-PH"] = {
    "languageTag": "fil-PH",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "₱",
        "code": "PHP"
    }
};
exports["fr-CA"] = {
    "languageTag": "fr-CA",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "M",
        "billion": "G",
        "trillion": "T"
    },
    "spaceSeparated": true,
    "currency": {
        "symbol": "$",
        "position": "postfix",
        "code": "USD"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "thousandSeparated": true,
            "mantissa": 2
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["fr-CH"] = {
    "languageTag": "fr-CH",
    "delimiters": {
        "thousands": " ",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "CHF",
        "position": "postfix",
        "code": "CHF"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["fr-FR"] = {
    "languageTag": "fr-FR",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "M",
        "billion": "Mrd",
        "trillion": "billion"
    },
    "bytes": {
        "binarySuffixes": [
            "o",
            "Kio",
            "Mio",
            "Gio",
            "Tio",
            "Pio",
            "Eio",
            "Zio",
            "Yio"
        ],
        "decimalSuffixes": [
            "o",
            "Ko",
            "Mo",
            "Go",
            "To",
            "Po",
            "Eo",
            "Zo",
            "Yo"
        ]
    },
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["he-IL"] = {
    "languageTag": "he-IL",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "אלף",
        "million": "מיליון",
        "billion": "מיליארד",
        "trillion": "טריליון"
    },
    "currency": {
        "symbol": "₪",
        "position": "prefix",
        "code": "ILS"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["hu-HU"] = {
    "languageTag": "hu-HU",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "E",
        "million": "M",
        "billion": "Mrd",
        "trillion": "T"
    },
    "currency": {
        "symbol": "Ft",
        "position": "postfix",
        "code": "HUF"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["id"] = {
    "languageTag": "id",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "r",
        "million": "j",
        "billion": "m",
        "trillion": "t"
    },
    "currency": {
        "symbol": "Rp",
        "code": "IDR"
    }
};
exports["it-CH"] = {
    "languageTag": "it-CH",
    "delimiters": {
        "thousands": "'",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "mila",
        "million": "mil",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "CHF",
        "code": "CHF"
    }
};
exports["it-IT"] = {
    "languageTag": "it-IT",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "mila",
        "million": "mil",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["ja-JP"] = {
    "languageTag": "ja-JP",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "千",
        "million": "百万",
        "billion": "十億",
        "trillion": "兆"
    },
    "currency": {
        "symbol": "¥",
        "position": "prefix",
        "code": "JPY"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "thousandSeparated": true,
            "mantissa": 2
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["ko-KR"] = {
    "languageTag": "ko-KR",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "천",
        "million": "백만",
        "billion": "십억",
        "trillion": "일조"
    },
    "currency": {
        "symbol": "₩",
        "code": "KPW"
    }
};
exports["lv-LV"] = {
    "languageTag": "lv-LV",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "tūkst.",
        "million": "milj.",
        "billion": "mljrd.",
        "trillion": "trilj."
    },
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["nb-NO"] = {
    "languageTag": "nb-NO",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "t",
        "million": "M",
        "billion": "md",
        "trillion": "b"
    },
    "currency": {
        "symbol": "kr",
        "position": "postfix",
        "code": "NOK"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["nb"] = {
    "languageTag": "nb",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "t",
        "million": "mil",
        "billion": "mia",
        "trillion": "b"
    },
    "currency": {
        "symbol": "kr",
        "code": "NOK"
    }
};
exports["nl-BE"] = {
    "languageTag": "nl-BE",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mln",
        "billion": "mld",
        "trillion": "bln"
    },
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["nl-NL"] = {
    "languageTag": "nl-NL",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "mln",
        "billion": "mrd",
        "trillion": "bln"
    },
    "currency": {
        "symbol": "€",
        "position": "prefix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["nn"] = {
    "languageTag": "nn",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "t",
        "million": "mil",
        "billion": "mia",
        "trillion": "b"
    },
    "currency": {
        "symbol": "kr",
        "code": "NOK"
    }
};
exports["pl-PL"] = {
    "languageTag": "pl-PL",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "tys.",
        "million": "mln",
        "billion": "mld",
        "trillion": "bln"
    },
    "currency": {
        "symbol": " zł",
        "position": "postfix",
        "code": "PLN"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["pt-BR"] = {
    "languageTag": "pt-BR",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "mil",
        "million": "milhões",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "R$",
        "position": "prefix",
        "code": "BRL"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["pt-PT"] = {
    "languageTag": "pt-PT",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "k",
        "million": "m",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["ro-RO"] = {
    "languageTag": "ro-RO",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "mii",
        "million": "mil",
        "billion": "mld",
        "trillion": "bln"
    },
    "currency": {
        "symbol": " lei",
        "position": "postfix",
        "code": "RON"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["ro"] = {
    "languageTag": "ro-RO",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "mii",
        "million": "mil",
        "billion": "mld",
        "trillion": "bln"
    },
    "currency": {
        "symbol": " lei",
        "position": "postfix",
        "code": "RON"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["ru-RU"] = {
    "languageTag": "ru-RU",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "тыс.",
        "million": "млн",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "руб.",
        "position": "postfix",
        "code": "RUB"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["ru-UA"] = {
    "languageTag": "ru-UA",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "тыс.",
        "million": "млн",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "₴",
        "position": "postfix",
        "code": "UAH"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["sk-SK"] = {
    "languageTag": "sk-SK",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "tis.",
        "million": "mil.",
        "billion": "mld.",
        "trillion": "bil."
    },
    "spaceSeparated": true,
    "currency": {
        "symbol": "€",
        "position": "postfix",
        "code": "EUR"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["sl"] = {
    "languageTag": "sl",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "tis.",
        "million": "mil.",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "€",
        "code": "EUR"
    }
};
exports["sr-Cyrl-RS"] = {
    "languageTag": "sr-Cyrl-RS",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "тыс.",
        "million": "млн",
        "billion": "b",
        "trillion": "t"
    },
    "currency": {
        "symbol": "RSD",
        "code": "RSD"
    }
};
exports["sv-SE"] = {
    "languageTag": "sv-SE",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "t",
        "million": "M",
        "billion": "md",
        "trillion": "tmd"
    },
    "currency": {
        "symbol": "kr",
        "position": "postfix",
        "code": "SEK"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["th-TH"] = {
    "languageTag": "th-TH",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "พัน",
        "million": "ล้าน",
        "billion": "พันล้าน",
        "trillion": "ล้านล้าน"
    },
    "currency": {
        "symbol": "฿",
        "position": "postfix",
        "code": "THB"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["tr-TR"] = {
    "languageTag": "tr-TR",
    "delimiters": {
        "thousands": ".",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "bin",
        "million": "milyon",
        "billion": "milyar",
        "trillion": "trilyon"
    },
    "currency": {
        "symbol": "₺",
        "position": "postfix",
        "code": "TRY"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "spaceSeparatedCurrency": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["uk-UA"] = {
    "languageTag": "uk-UA",
    "delimiters": {
        "thousands": " ",
        "decimal": ","
    },
    "abbreviations": {
        "thousand": "тис.",
        "million": "млн",
        "billion": "млрд",
        "trillion": "блн"
    },
    "currency": {
        "symbol": "₴",
        "position": "postfix",
        "code": "UAH"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "output": "currency",
            "mantissa": 2,
            "spaceSeparated": true,
            "thousandSeparated": true
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "spaceSeparated": true,
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["zh-CN"] = {
    "languageTag": "zh-CN",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "千",
        "million": "百万",
        "billion": "十亿",
        "trillion": "兆"
    },
    "currency": {
        "symbol": "¥",
        "position": "prefix",
        "code": "CNY"
    },
    "currencyFormat": {
        "thousandSeparated": true,
        "totalLength": 4,
        "spaceSeparated": true,
        "average": true
    },
    "formats": {
        "fourDigits": {
            "totalLength": 4,
            "spaceSeparated": true,
            "average": true
        },
        "fullWithTwoDecimals": {
            "thousandSeparated": true,
            "mantissa": 2
        },
        "fullWithTwoDecimalsNoCurrency": {
            "mantissa": 2,
            "thousandSeparated": true
        },
        "fullWithNoDecimals": {
            "output": "currency",
            "thousandSeparated": true,
            "mantissa": 0
        }
    }
};
exports["zh-MO"] = {
    "languageTag": "zh-MO",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "千",
        "million": "百萬",
        "billion": "十億",
        "trillion": "兆"
    },
    "currency": {
        "symbol": "MOP",
        "code": "MOP"
    }
};
exports["zh-SG"] = {
    "languageTag": "zh-SG",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "千",
        "million": "百万",
        "billion": "十亿",
        "trillion": "兆"
    },
    "currency": {
        "symbol": "$",
        "code": "SGD"
    }
};
exports["zh-TW"] = {
    "languageTag": "zh-TW",
    "delimiters": {
        "thousands": ",",
        "decimal": "."
    },
    "abbreviations": {
        "thousand": "千",
        "million": "百萬",
        "billion": "十億",
        "trillion": "兆"
    },
    "currency": {
        "symbol": "NT$",
        "code": "TWD"
    }
};